#include "crt.h"

void * malloc(size_t size)
{
	return HeapAlloc(
		hProcessHeap,
		HEAP_GENERATE_EXCEPTIONS,
		size
	);
}

void * zalloc(unsigned int size)
{
	return HeapAlloc(
		hProcessHeap,
		HEAP_GENERATE_EXCEPTIONS |
		HEAP_ZERO_MEMORY,
		size
	);
}

void free(void * data)
{
	HeapFree(hProcessHeap, 0, data);
}
