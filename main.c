#include "crt.h"
#include "alloc.h"

static void handle_error()
{
	DWORD  dwError = GetLastError();
	DWORD  dwBytesWritten;
	DWORD  szMsgBuf = sizeof(TCHAR) * 1024;
	LPTSTR strMsgBuf = malloc(szMsgBuf);

	FormatMessage(
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, dwError, 0,
		strMsgBuf, szMsgBuf,
		NULL
	);

	WriteFile(
		hStdOut,
		strMsgBuf, szMsgBuf,
		&dwBytesWritten, NULL
	);

	CloseHandle(hStdOut);
fail_stdout:
	free(strMsgBuf);
}

int wmain()
{
	int return_code;
	BOOL success;

	LPVOID lpInBuf = malloc(1024);
	LPVOID lpOutBuf = malloc(1024);

	DWORD dwOutRead;
	DWORD dwOutConsole;

	const TCHAR PIPE_NAME[] = TEXT("\\\\.\\pipe\\this_is_a_test");

	HANDLE hPipe = CreateNamedPipe(
		PIPE_NAME,
		PIPE_ACCESS_DUPLEX,
		PIPE_TYPE_BYTE | PIPE_READMODE_BYTE,
		PIPE_UNLIMITED_INSTANCES,
		1024, 1024, 0, NULL
	);

	if (hPipe == INVALID_HANDLE_VALUE) {
		handle_error();
		return_code = 2;
		goto fail_create;
	}

	success = ConnectNamedPipe(hPipe, NULL);

	if (success == FALSE) {
		handle_error();
		return_code = 3;
		goto fail_connect;
	}

	success = ReadFile(
		hPipe, lpInBuf,
		1024, &dwOutRead,
		NULL
	);

	if (success == FALSE) {
		handle_error();
		return_code = 4;
		goto fail_read;
	}

	WriteFile(
		hStdOut,
		lpInBuf, dwOutRead,
		&dwOutConsole, NULL
	);

	return_code = 0;

fail_read:
fail_connect:
	CloseHandle(hPipe);
fail_create:
	free(lpInBuf);
	free(lpOutBuf);
	return return_code;
}