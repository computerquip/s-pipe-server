# Why?
You know how you see that program that's 50mB and you wonder what all it entails... and you realize that it does nothing close to 50mB worth of work?
A lot of programs and libraries now adays have implicit dependencies on things like the C runtimes and C++ runtimes. UI applications go further in that they practically package an entire operating system's worth of interfaces into their libraries, which reflects in both their size, complexity, and often their performance. In even further recent days, we've begun shipping entire browsers to drive our applications, such as Chromium, which *actually* ship an entire operating system worth of libraries.

At some point, I realized that there's a ridiculous amount of complexity and verbosity there to do even the most simple of tasks.

These programs, which I'm prefixing with `s-` for `stupid-`, are here to show that with a mild amount of effort, you can accomplish great things using nothing but simple OS-based interfaces and a bit of forgotten low-level knowledge. This program right now does nothing but start a synchronous named pipe, waits for a connection, reads a message, then prints the message. The code size reflects that and so does the binary size (usually at less than 10kB which the extremist might say is casual-grade I'm sure).

# Build Instructions
This particular project requires three things:
1. Windows
	* Preferably from the last 20 years but not required.
2. A compiler toolchain
	* Any GNU-style toolchain should work as long as it works with C90.
	* Any MSVC toolchain that I know of, as long is it compiles compliant C90.
3. A make binary
	* Seriously, the only dependency-based build system I can find for Windows that runs outside of nmake.
	* Find a copy here: https://sourceforge.net/projects/ezwinports/files/make-4.2.1-without-guile-w32-bin.zip
	* Or just use one that comes with a MinGW installation.
	* Or just compile your own.
	* I'm not sure the minimum version. Probably 3.x or later should be fine.

There's two makefiles currently.
1. `Makefile.mingw` - Should probably be `Makefile.gnu` but I'm too lazy to change it.
2. `Makefile.msvc` - Should work with an msvc environment. Assumes that you've already set msvc environment variables.

Run them however you want. It will place object files and executable inside of an `out` folder. `make clean` simply deletes that folder.
You can set CC and LD when using `Makefile.mingw`. `Makefile.msvc` assumes that cl.exe and link.exe is already in your path (since there's no viable way to detect a MSVC installation).


## Why Make?
Because it's the only thing I could get to reliably run, wasn't specific to a toolchain (like nmake), and doesn't have a horribly complicated environment. I'd like to use Tup which I feel is more legible and friendly but I can't get it to run on Windows unfortunately so whatever.