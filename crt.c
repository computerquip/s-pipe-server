#include "crt.h"

#include <shellapi.h> /* // CommandLineToArgvWf */

int wmain(int argc, wchar_t *argv[]);

HANDLE hProcessHeap;
HANDLE hStdOut, hStdIn, hStdErr;

int wmainCRTStartup()
{
	int iNumArgs;
	int iReturnCode;
	LPWSTR strCommandLine;
	LPWSTR *strArguments;
	DWORD dwUnused;

	hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);

	if (hStdOut == INVALID_HANDLE_VALUE)
		ExitProcess(-1);

	hStdErr = GetStdHandle(STD_ERROR_HANDLE);

	if (hStdErr == INVALID_HANDLE_VALUE)
		ExitProcess(-2);

	hStdIn = GetStdHandle(STD_INPUT_HANDLE);

	if (hStdIn == INVALID_HANDLE_VALUE)
		ExitProcess(-3);

	hProcessHeap = HeapCreate(0, 0, 0);

	if (hProcessHeap == INVALID_HANDLE_VALUE)
		ExitProcess(-4);

	strCommandLine = GetCommandLineW();

	if (strCommandLine == NULL)
		ExitProcess(-5);

	strArguments = CommandLineToArgvW(strCommandLine, &iNumArgs);

	if (strArguments == NULL)
		ExitProcess(-6);

	iReturnCode = wmain(iNumArgs, strArguments);

	free(strArguments);

	CloseHandle(hStdIn);
	CloseHandle(hStdErr);
	CloseHandle(hStdOut);
	HeapDestroy(hProcessHeap);
	ExitProcess(iReturnCode);
}