#pragma once

void * malloc(size_t size);
void * zalloc(unsigned int size);
void free(void * data);
